
difference(){
  cube([20, 20, 20], center=true);
  
  cylinder(21, 10, 5, center = true, $fn=180);
  hole();
  mirror([1, 0, 0]) hole();
  mirror([0, 1, 0]) hole();
  mirror([0, 1, 0]) { 
    mirror([1, 0, 0]) hole();
  }
}

module hole(){
  translate([7, 7, 0]){
    cylinder(21, 1, center=true, $fn=45);
  }
}
